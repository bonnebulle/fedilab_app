---
title: How to login
date: 2018-09-02
---

## How to login

Sometimes people are asking how to login to their account. This post will try to clear this procedure.

These steps are made from a fresh install of Fedilab, so let’s start!


### Step 1 – Open Fedilab App

You are asked to insert your instance server address (see note A for what’s an instance), email address used to register an account there and the password you chose for it.


{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/01.png" caption="First Login Screen">}}
{{< /gallery >}}
{{< load-photoswipe >}}

Of course you have this data if you already have an account in the fediverse! (see note B for what’s the fediverse)
If you don’t have one already, keep following the next steps!
If you have one, skip to **step 4**.


### Step 2 – Choose an instance

Press “**no account yet?**” link right under the logo: it will open a webpage that will help you to choose an instance that fits your interests and preferences.
{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/02.png" caption="Choose an instance">}}
{{< /gallery >}}
{{< load-photoswipe >}}

You can choose language accepted, how much people are on there, how it is moderated (or better which behaviours are accepted or not).

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/03.png" caption="Choose language">}}
   {{< figure link="../img/how-to-login/04.png" caption="Choose size">}}
    {{< figure link="../img/how-to-login/05.png" caption="Choose content">}}
{{< /gallery >}}
{{< load-photoswipe >}}

It will now show you a list of instances that match your choices. Pick one that you like (i chose a random one), and then press “**go to instance**“.

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/06.png" caption="List of instances">}}
   {{< figure link="../img/how-to-login/07.png" caption="Pick one">}}
{{< /gallery >}}
{{< load-photoswipe >}}

### Step 3 – Register an account

Now you just have to sign up to the instance you have chosen like a normal website putting an username, an email address for confirmation and twice a password.
You could see 2 kind of interfaces, Mastodon or Pleroma (see note C for more info).

If your instance runs Mastodon just scroll down until you see the registration fields.

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/08.png" caption="Mastodon Signup">}}
{{< /gallery >}}
{{< load-photoswipe >}}

If it runs Pleroma click on the top-right button (the one with the user icon with the + near), or press “**Sidebar**” then “**Register**“.

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/09.png" caption="Pleroma Signup">}}
{{< /gallery >}}
{{< load-photoswipe >}}

**Tip**: Choose wisely your username because it can’t be changed (it’s like an email address).
When you will be inside, you will be able to edit your ‘nickname’/’title’ as you wish, but this is another story…

**IMPORTANT**: If you are on Mastodon instance check your inbox/spam for verification email in order to activate your account!

### Step 4 – Login

Now we have all the required data to go in! Fill all the fields that we in the first step.

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/13.png" caption="Fill the form">}}
{{< /gallery >}}
{{< load-photoswipe >}}

Note: before you press “**login**“, if you eventually are behind a proxy, you can config its data for connection: go in the top-right angle menu, choose “**Proxy**” and insert your data.

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/14.png" caption="Login menu">}}
   {{< figure link="../img/how-to-login/15.png" caption="Proxy details">}}
{{< /gallery >}}
{{< load-photoswipe >}}


Now you can press “**login**“.


One last popup: the app will ask your preferences about notifications (they can be completely disabled or more tuned in the settings page).

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/16.png" caption="Notification preferences">}}
{{< /gallery >}}
{{< load-photoswipe >}}

And you’re in!

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/17.png" caption="App Menu">}}
{{< /gallery >}}
{{< load-photoswipe >}}

You can add as many account as you want/have. Just press your name and press “**Add another account**“.


{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/how-to-login/18.png" caption="Multi-account">}}
{{< /gallery >}}
{{< load-photoswipe >}}

### Troubleshooting

If you can’t login, and you double checked your instance name/email/password, try open a webpage and check if you can reach your instance (it’s a valid suggestion, i had this problem too while doing this article, so…).
If you are sure you did everything in the right way, press “**Authentication does not work?**” then click the link at the bottom and open an issue.

## Notes
Note A: instances are servers, just like email servers (e.g. gmail, hotmail, …). You can register in one (gmail) and send email to the others (to hotmail) right?!
So, here it’s the same, you register in one instance and you can talk to people that have accounts in other instances.

Note B: Fediverse is an hypotetic web of servers that talk each other the same language (ActivityPub). Everyone can set up his own and talk to the others.
It’s the opposite of the most famous social networks, that you register in one server (e.g. twitter) and you can talk only within that server (you can’t talk directly with facebook people for example).

Note C: Mastodon and Pleroma are two different server software for the fediverse.
It’s like a web browser, some people like Firefox, other people like Chrome, but all the people can surf the web.
The same applies to Mastodon and Pleroma, features and interface may slightly differ, but both are great and with them you can talk with other people!


#### Article from [@metalbiker](https://ins.mastalab.app/@metalbiker)