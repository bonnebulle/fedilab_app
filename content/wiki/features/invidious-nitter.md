*[Home](../../home) > [Features](../../features) &gt;* Invidious and Nitter

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## What is Invidious?

Invidious is an alternative front-end to YouTube that does respect your privacy. When enabled, all links to Youtube will be 
automatically replaced with invidio.us URLs. The instance can be customised in settings (see below for more instances).

Source: [Github](https://github.com/omarroth/invidious)

Official instance: [invidio.us](https://invidio.us/)

## What is Nitter?

Nitter is a free and open source alternative Twitter front-end focused on privacy. It has been inspired by the invidio.us project.
In a same way, when this feature is enabled, all Twitter links will be replaced with nitter.net URLs. The instance can be customised too.

Source: [Github](https://github.com/zedeus/nitter)

Official instance: [nitter.net](https://nitter.net/)

<div id="instances"></div>

## What are available instances?

### Invidious instances

- [invidious.snopyta.org](https://invidious.snopyta.org)
- [vid.wxzm.sx](https://vid.wxzm.sx)
- [invidious.kabi.tk](https://invidious.kabi.tk)
- [invidiou.sh](https://invidiou.sh)
- [no.invidiou.sh](https://no.invidiou.sh)
- [tube.poal.co](https://tube.poal.co)
- [invidious.13ad.de](https://invidious.13ad.de)
- [yt.elukerio.org](https://yt.elukerio.org)
- [yt.lelux.fi](https://yt.lelux.fi)


### Nitter instances

- [nitter.snopyta.org](https://nitter.snopyta.org)
- [nitter.42l.fr](https://nitter.42l.fr)
- [nitter.nixnet.xyz](https://nitter.nixnet.xyz)
- [nitter.lelux.fi](https://nitter.lelux.fi)